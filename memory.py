import random
import time
from tkinter import Tk, Button, DISABLED

def toon_symbool(x, y):
    global eerste
    global vorigeX, vorigeY
    knoppen[x, y]['text'] = knop_symbolen[x, y]
    knoppen[x, y].update_idletasks()
    
    if eerste:
        vorigeX = x
        vorigeY = y
        eerste = False
    elif vorigeX != x or vorigeY != y:
        if knoppen[vorigeX, vorigeY]['text'] != knoppen[x, y]['text']:
            time.sleep(0.5)
            knoppen[vorigeX, vorigeY]['text'] = ''
            knoppen[x, y]['text'] = ''
        else:
            knoppen[vorigeX, vorigeY]['command'] = DISABLED
            knoppen[x, y]['command'] = DISABLED
        eerste = True

root = Tk()
root.title('Memory')
root.resizable(width=False, height=False)

knoppen = {}
eerste = True
vorigeX = 0
vorigeY = 0

knop_symbolen = {}

symbolen = [u'\u2702', u'\u2702', u'\u2705', u'\u2705', u'\u2708', u'\u2708',
            u'\u2709', u'\u2709', u'\u270A', u'\u270A', u'\u270B', u'\u270B',
            u'\u270C', u'\u270C', u'\u270F', u'\u270F', u'\u2712', u'\u2712',
            u'\u2714', u'\u2714', u'\u2716', u'\u2716', u'\u2728', u'\u2728']

random.shuffle(symbolen)

for x in range(6):
    for y in range(4):
        knop = Button(command=lambda x=x, y=y: toon_symbool(x, y), width=3, height=3)
        knop.grid(column=x, row=y)
        knoppen[x, y] = knop
        knop_symbolen[x, y] = symbolen.pop()

root.mainloop()