import random

levens = 9
woorden = ['pizza', 'elfje', 'toets', 'shirt', 'otter', 'plein']
geheime_woord = random.choice(woorden)
hint = list('?????')
hart_symbool = u'\u2764'
geraden_woord_juist = False

def update_hint(geraden_letter, geheime_woord, hint):
    index = 0
    while index < len(geheime_woord):
        if geraden_letter == geheime_woord[index]:
            hint[index] = geraden_letter
        index = index + 1

while levens > 0:
    print(hint)
    print('Levens over: ' + hart_symbool * levens)
    raad = input('Raad een letter of het hele woord: ')

    if raad == geheime_woord:
        geraden_woord_juist = True
        break

    if raad in geheime_woord:
        update_hint(raad, geheime_woord, hint)
    else:
        print('Fout. Je verliest een leven')
        levens = levens - 1

if geraden_woord_juist:
    print('Gewonnen! Het geheime woord was ' \
    + geheime_woord)
else:
    print('Verloren! Het geheime woord was ' \
    + geheime_woord)