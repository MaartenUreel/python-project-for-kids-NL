import turtle as t
from random import randint, random

def teken_ster(punten, grootte, kleur, x, y):
    t.penup()
    t.goto(x, y)
    t.pendown()
    hoek = 180 - (180 / punten)
    t.color(kleur)
    t.begin_fill()
    for i in range(punten):
        t.forward(grootte)
        t.right(hoek)
    t.end_fill()

# Hoofdcode
t.Screen().bgcolor('dark blue')

while True:
    willekeurig_punten = randint(2, 5) * 2 + 1
    willekeurig_grootte = randint(10, 50)
    willekeurig_kleur = (random(), random(), random())
    willekeurig_x = randint(-350, 300)
    willekeurig_y = randint(-250, 250)

    teken_ster(willekeurig_punten, willekeurig_grootte, willekeurig_kleur, willekeurig_x, willekeurig_y)
