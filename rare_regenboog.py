import random
import turtle as t

def verkrijg_lijn_lengte():
    keuze = input('Voer de lengte voor de lijn in (lang, gemiddeld, kort): ')
    if keuze == 'lang':
        lijn_lengte = 250
    elif keuze == 'gemiddeld':
        lijn_lengte = 200
    else:
        lijn_lengte = 100
    return lijn_lengte

def verkrijg_lijn_dikte():
    keuze = input('Voer lijndikte in (superdik, dik, dun): ')
    if keuze == 'superdik':
        lijn_dikte = 40
    elif keuze == 'dik':
        lijn_dikte = 25
    else:
        lijn_dikte = 10
    return lijn_dikte

def in_venster():
    limiet_links = (-t.window_width() / 2) + 100
    limiet_rechts = (t.window_width() / 2) - 100
    limiet_boven = (t.window_height() / 2) - 100
    limiet_onder = (-t.window_height() / 2) + 100
    (x, y) = t.pos()
    binnen = limiet_links < x < limiet_rechts and limiet_onder < y < limiet_boven
    return binnen

def verplaats_turtle(lijn_lengte):
    pen_kleuren = ['red', 'orange', 'yellow', 'green', 'blue', 'purple']
    t.pencolor(random.choice(pen_kleuren))
    if in_venster():
        hoek = random.randint(0, 180)
        t.right(hoek)
        t.forward(lijn_lengte)
    else:
        t.backward(lijn_lengte)

lijn_lengte = verkrijg_lijn_lengte()
lijn_dikte = verkrijg_lijn_dikte()

t.shape('turtle')
t.fillcolor('green')
t.bgcolor('black')
t.speed('fastest')
t.pensize(lijn_dikte)

while True:
    verplaats_turtle(lijn_lengte)