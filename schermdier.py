from tkinter import HIDDEN, NORMAL, Tk, Canvas

def toggle_ogen():
    huidig_kleur = c.itemcget(oog_links, 'fill')
    nieuwe_kleur = c.lijf_kleur if huidig_kleur == 'white' else 'white'
    huidige_status = c.itemcget(pupil_links, 'state')
    nieuwe_status = NORMAL if huidige_status == HIDDEN else HIDDEN
    c.itemconfigure(pupil_links, state=nieuwe_status)
    c.itemconfigure(pupil_rechts, state=nieuwe_status)
    c.itemconfigure(oog_links, fill=nieuwe_kleur)
    c.itemconfigure(oog_rechts, fill=nieuwe_kleur)

def knipperen():
    toggle_ogen()
    root.after(250, toggle_ogen)
    root.after(3000, knipperen)

def toggle_pupillen():
    if not c.scheel:
        c.move(pupil_links, 10, -5)
        c.move(pupil_rechts, -10, -5)
        c.scheel = True
    else:
        c.move(pupil_links, -10, 5)
        c.move(pupil_rechts, 10, 5)
        c.scheel = False

def toggle_tong():
    if not c.tong_uit:
        c.itemconfigure(tong_tip, state=NORMAL)
        c.itemconfigure(tong_hoofd, state=NORMAL)
        c.tong_uit = True
    else:
        c.itemconfigure(tong_tip, state=HIDDEN)
        c.itemconfigure(tong_hoofd, state=HIDDEN)
        c.tong_uit = False

def brutaal(event):
    toggle_tong()
    toggle_pupillen()
    verberg_blij(event)
    root.after(1000, toggle_tong)
    root.after(1000, toggle_pupillen)
    return

def toon_blij(event):
    if (20 <= event.x and event.x <= 350) and (20 <= event.y and event.y <= 350):
        c.itemconfigure(wang_links, state=NORMAL)
        c.itemconfigure(wang_rechts, state=NORMAL)
        c.itemconfigure(mond_blij, state=NORMAL)
        c.itemconfigure(mond_normaal, state=HIDDEN)
        c.itemconfigure(mond_sip, state=HIDDEN)
        c.blij = 10
    return

def verberg_blij(event):
    c.itemconfigure(wang_links, state=HIDDEN)
    c.itemconfigure(wang_rechts, state=HIDDEN)
    c.itemconfigure(mond_blij, state=HIDDEN)
    c.itemconfigure(mond_normaal, state=NORMAL)
    c.itemconfigure(mond_sip, state=HIDDEN)
    return

def verdrietig():
    if c.blij == 0:
        c.itemconfigure(mond_blij, state=HIDDEN)
        c.itemconfigure(mond_normaal, state=HIDDEN)
        c.itemconfigure(mond_sip, state=NORMAL)
    else:
        c.blij -= 1
        root.after(5000, verdrietig)

root = Tk()
c = Canvas(root, width=400, height=400)
c.configure(bg='dark blue', highlightthickness=0)
c.lijf_kleur = 'SkyBlue1'
lijf = c.create_oval(35, 20, 365, 350, outline=c.lijf_kleur, fill=c.lijf_kleur)
oor_links = c.create_polygon(75, 80, 75, 10, 165, 70, outline=c.lijf_kleur, fill=c.lijf_kleur)
oor_rechts = c.create_polygon(255, 45, 325, 10, 320, 70, outline=c.lijf_kleur, fill=c.lijf_kleur)
voet_links = c.create_oval(65, 320, 145, 360, outline=c.lijf_kleur, fill=c.lijf_kleur)
voet_rechts = c.create_oval(250, 320, 330, 360, outline=c.lijf_kleur, fill=c.lijf_kleur)

oog_links = c.create_oval(130, 110, 160, 170, outline='black', fill='white')
pupil_links = c.create_oval(140, 145, 150, 155, outline='black', fill='black')
oog_rechts = c.create_oval(230, 110, 260, 170, outline='black', fill='white')
pupil_rechts = c.create_oval(240, 145, 250, 155, outline='black', fill='black')

mond_normaal = c.create_line(170, 250, 200, 272, 230, 250, smooth=1, width=2, state=NORMAL)
mond_blij = c.create_line(170, 250, 200, 282, 230, 250, smooth=1, width=2, state=HIDDEN)
mond_sip = c.create_line(170, 250, 200, 232, 230, 250, smooth=1, width=2, state=HIDDEN)
tong_hoofd = c.create_rectangle(170, 250, 230, 290, outline='red', fill='red', state=HIDDEN)
tong_tip = c.create_oval(170, 285, 230, 300, outline='red', fill='red', state=HIDDEN)

wang_links = c.create_oval(70, 180, 120, 230, outline='pink', fill='pink', state=HIDDEN)
wang_rechts = c.create_oval(280, 180, 330, 230, outline='pink', fill='pink', state=HIDDEN)

c.pack()

c.bind('<Motion>', toon_blij)
c.bind('<Leave>', verberg_blij)
c.bind('<Double-1>', brutaal)

c.blij = 10
c.scheel = False
c.tong_uit = False

root.after(1000, knipperen)
root.after(5000, verdrietig)
root.mainloop()