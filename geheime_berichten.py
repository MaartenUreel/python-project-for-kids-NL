from tkinter import messagebox, simpledialog, Tk

def is_even(getal):
    return getal % 2 == 0

def ontvang_even_letters(bericht):
    even_letters = []
    for teller in range(0, len(bericht)):
        if is_even(teller):
            even_letters.append(bericht[teller])
    return even_letters

def ontvang_oneven_letters(bericht):
    oneven_letters = []
    for teller in range(0, len(bericht)):
        if not is_even(teller):
            oneven_letters.append(bericht[teller])
    return oneven_letters

def wissel_letters(bericht):
    letter_lijst = []
    if not is_even(len(bericht)):
        bericht = bericht + 'x'
    even_letters = ontvang_even_letters(bericht)
    oneven_letters = ontvang_oneven_letters(bericht)
    for teller in range(0, int(len(bericht)/2)):
        letter_lijst.append(oneven_letters[teller])
        letter_lijst.append(even_letters[teller])
    nieuw_bericht = ''.join(letter_lijst)
    return nieuw_bericht

def ontvang_taak():
    taak = simpledialog.askstring('Taak', 'Wil je coderen of decoderen?')
    return taak

def ontvang_bericht():
    bericht = simpledialog.askstring('Bericht', 'Voer het geheime bericht in: ')
    return bericht

root = Tk()

while True:
    taak = ontvang_taak()
    if taak == 'coderen':
        bericht = ontvang_bericht()
        gecodeerd = wissel_letters(bericht)
        messagebox.showinfo('Ciphertekst van het geheime bericht is:', gecodeerd)
    elif taak == 'decoderen':
        bericht = ontvang_bericht()
        gedecodeerd = wissel_letters(bericht)
        messagebox.showinfo('Platte tekst van het geheime bericht is:', gedecodeerd)
    else:
        break
root.mainloop()