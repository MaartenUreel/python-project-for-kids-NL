from itertools import cycle
from random import randrange
from tkinter import Canvas, Tk, messagebox, font

canvas_breedte = 800
canvas_hoogte = 400

root = Tk()
c = Canvas(root, width=canvas_breedte, height=canvas_hoogte, \
background='deep sky blue')
c.create_rectangle(-5, canvas_hoogte - 100, canvas_breedte + 5, \
canvas_hoogte + 5, fill='sea green', width=0)
c.create_oval(-80, -80, 120, 120, fill='orange', width=0)
c.pack()

kleur_cyclus = cycle(['light blue', 'light green', 'light pink', 'light yellow', 'light cyan'])
ei_breedte = 45
ei_hoogte = 55
ei_score = 10
ei_snelheid = 500
ei_interval = 4000
moeilijkheidsgraad = 0.95

vanger_kleur = 'blue'
vanger_breedte = 100
vanger_hoogte = 100
vanger_start_x = canvas_breedte / 2 - vanger_breedte / 2
vanger_start_y = canvas_hoogte - vanger_hoogte - 20
vanger_start_x2 = vanger_start_x + vanger_breedte
vanger_start_y2 = vanger_start_y + vanger_hoogte

vanger = c.create_arc(vanger_start_x, vanger_start_y, \
                      vanger_start_x2, vanger_start_y2, start=200, extent=140, \
                      style='arc', outline=vanger_kleur, width=3)

spel_lettertype = font.nametofont('TkFixedFont')
spel_lettertype.config(size=18)

score = 0
score_tekst = c.create_text(10, 10, anchor='nw', font=spel_lettertype, fill='darkblue', \
                            text='Score: ' + str(score))

resterende_levens = 3
levens_tekst = c.create_text(canvas_breedte - 10, 10, anchor='ne', font=spel_lettertype, \
                             fill='darkblue', text='Levens: ' + str(resterende_levens))

eieren = []

def maak_ei():
    x = randrange(10, 740)
    y = 40
    nieuw_ei = c.create_oval(x, y, x + ei_breedte, y + ei_hoogte, fill=next(kleur_cyclus), width=0)
    eieren.append(nieuw_ei)
    root.after(ei_interval, maak_ei)

def beweeg_eieren():
    for ei in eieren:
        (ei_x, ei_y, ei_x2, ei_y2) = c.coords(ei)
        c.move(ei, 0, 10)
        if ei_y2 > canvas_hoogte:
            ei_gevallen(ei)
        
    root.after(ei_snelheid, beweeg_eieren)

def ei_gevallen(ei):
    eieren.remove(ei)
    c.delete(ei)
    verlies_een_leven()
    if resterende_levens == 0:
        messagebox.showinfo('Game Over!', 'Eindscore: ' \
                            + str(score))
    root.destroy()

def verlies_een_leven():
    global resterende_levens
    resterende_levens -= 1
    c.itemconfigure(levens_tekst, text='Levens: ' \
                    + str(resterende_levens))

def controleer_vangst():
    (vanger_x, vanger_y, vanger_x2, vanger_y2) = c.coords(vanger)
    for ei in eieren:
        (ei_x, ei_y, ei_x2, ei_y2) = c.coords(ei)
        if vanger_x < ei_x and ei_x2 < vanger_x2 and vanger_y2 - ei_y2 < 40:
            eieren.remove(ei)
            c.delete(ei)
            verhoog_score(ei_score)
    root.after(100, controleer_vangst)

def verhoog_score(punten):
    global score, ei_snelheid, ei_interval
    score += punten
    ei_snelheid = int(ei_snelheid * moeilijkheidsgraad)
    ei_interval = int(ei_interval * moeilijkheidsgraad)
    c.itemconfigure(score_tekst, text='Score: ' + str(score))

def beweeg_links(event):
    (x1, y1, x2, y2) = c.coords(vanger)
    if x1 > 0:
        c.move(vanger, -20, 0)

def beweeg_rechts(event):
    (x1, y1, x2, y2) = c.coords(vanger)
    if x2 < canvas_breedte:
        c.move(vanger, 20, 0)

c.bind('<Left>', beweeg_links)
c.bind('<Right>', beweeg_rechts)
c.focus_set()

root.after(1000, maak_ei)
root.after(1000, beweeg_eieren)
root.after(1000, controleer_vangst)
root.mainloop()