import random

FONT_KLEUR = (255, 255, 255)
WIDTH = 800
HEIGHT = 600
MIDDEN_X = WIDTH / 2
MIDDEN_Y = HEIGHT / 2
MIDDEN = (MIDDEN_X, MIDDEN_Y)
LAATSTE_LEVEL = 6
START_SNELHEID = 10
KLEUREN = ['green', 'blue']

game_over = False
game_voltooid = False
huidig_level = 1
sterren = []
animaties = []


def draw():
    global sterren, huidig_level, game_over, game_voltooid
    screen.clear()
    screen.blit('space', (0, 0))
    if game_over:
        toon_boodschap('GAME OVER!', 'Probeer nogmaals.')
    elif game_voltooid:
        toon_boodschap('JE HEBT GEWONNEN!', 'GOED GEDAAN.')
    else:
        for ster in sterren:
            ster.draw()


def update():
    global sterren
    if len(sterren) == 0:
        sterren = maak_nieuwe_sterren(huidig_level)


def maak_nieuwe_sterren(aantal_extra_sterren):
    kleuren_te_maken = krijg_kleuren_te_maken(aantal_extra_sterren)
    nieuwe_sterren = maak_sterren(kleuren_te_maken)
    layout_sterren(nieuwe_sterren)
    animeer_sterren(nieuwe_sterren)
    return nieuwe_sterren


def krijg_kleuren_te_maken(aantal_extra_sterren):
    kleuren_te_maken = ['red']
    for i in range(0, aantal_extra_sterren):
        random_kleur = random.choice(KLEUREN)
        kleuren_te_maken.append(random_kleur)
    return kleuren_te_maken


def maak_sterren(kleuren_te_maken):
    nieuwe_sterren = []
    for kleur in kleuren_te_maken:
        ster = Actor(kleur + '-star')
        nieuwe_sterren.append(ster)
    return nieuwe_sterren


def layout_sterren(sterren_layout):
    aantal_openingen = len(sterren_layout) + 1
    opening_grootte = WIDTH / aantal_openingen
    random.shuffle(sterren_layout)
    for index, ster in enumerate(sterren_layout):
        nieuw_x_pos = (index + 1) * opening_grootte
        ster.x = nieuw_x_pos


def animeer_sterren(sterren_te_animeren):
    for ster in sterren_te_animeren:
        duur = START_SNELHEID - huidig_level
        ster.anchor = ('center', 'bottom')
        animatie = animate(ster, duration=duur, on_finished=handle_game_over, y=HEIGHT)
        animaties.append(animatie)


def handle_game_over():
    global game_over
    game_over = True


def on_mouse_down(pos):
    global sterren, huidig_level
    for ster in sterren:
        if ster.collidepoint(pos):
            if 'red' in ster.image:
                red_ster_klik()
            else:
                handle_game_over()


def red_ster_klik():
    global huidig_level, sterren, animaties, game_voltooid
    stop_animaties(animaties)
    if huidig_level == LAATSTE_LEVEL:
        game_voltooid = True
    else:
        huidig_level = huidig_level + 1
        sterren = []
        animaties = []


def stop_animaties(animaties_stoppen):
    for animatie in animaties_stoppen:
        if animatie.running:
            animatie.stop()


def toon_boodschap(titeltekst, subtiteltekst):
    screen.draw.text(titeltekst, fontsize=60, center=MIDDEN, color=FONT_KLEUR)
    screen.draw.text(subtiteltekst,
                     fontsize=30,
                     center=(MIDDEN_X, MIDDEN_Y + 30),
                     color=FONT_KLEUR)
