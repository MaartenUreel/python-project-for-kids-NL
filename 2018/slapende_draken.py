import math

WIDTH = 800
HEIGHT = 600
MIDDEN_X = WIDTH / 2
MIDDEN_Y = HEIGHT / 2
MIDDEN = (MIDDEN_X, MIDDEN_Y)
FONT_KLEUR = (0, 0, 0)
EIEREN_DOEL = 20
HELD_START = (200, 300)
AANVAL_AFSTAND = 200
DRAAK_WEKTIJD = 2
EIEREN_VERBERGTIJD = 2
BEWEEG_AFSTAND = 5

levens = 3
eieren_verzameld = 0
game_over = False
game_voltooid = False
reset_vereist = False

makkelijke_grot = {
    'draak': Actor('dragon-asleep', pos=(600, 100)),
    'eieren': Actor('one-egg', pos=(400, 100)),
    'ei_tellen': 1,
    'ei_verborgen': False,
    'ei_verberg_teller': 0,
    'slaap_lengte': 10,
    'slaap_teller': 0,
    'wakker_teller': 0
}
medium_grot = {
    'draak': Actor('dragon-asleep', pos=(600, 300)),
    'eieren': Actor('two-eggs', pos=(400, 300)),
    'ei_tellen': 2,
    'ei_verborgen': False,
    'ei_verberg_teller': 0,
    'slaap_lengte': 7,
    'slaap_teller': 0,
    'wakker_teller': 0
}
moeilijke_grot = {
    'draak': Actor('dragon-asleep', pos=(600, 500)),
    'eieren': Actor('three-eggs', pos=(400, 500)),
    'ei_tellen': 3,
    'ei_verborgen': False,
    'ei_verberg_teller': 0,
    'slaap_lengte': 4,
    'slaap_teller': 0,
    'wakker_teller': 0
}
grotten = [makkelijke_grot, medium_grot, moeilijke_grot]
held = Actor('hero', pos=HELD_START)


def draw():
    global grotten, eieren_verzameld, levens, game_voltooid
    screen.clear()
    screen.blit('dungeon', (0, 0))
    if game_over:
        screen.draw.text('GAME OVER!', fontsize=60, center=MIDDEN, color=FONT_KLEUR)
    elif game_voltooid:
        screen.draw.text('JE HEBT GEWONNEN!', fontsize=60, center=MIDDEN, color=FONT_KLEUR)
    else:
        held.draw()
        teken_grotten(grotten)
        teken_tellers(eieren_verzameld, levens)


def teken_grotten(grotten_te_tekenen):
    for grot in grotten_te_tekenen:
        grot['draak'].draw()
        if grot['ei_verborgen'] is False:
            grot['eieren'].draw()


def teken_tellers(eieren_verzameld, levens):
    screen.blit('egg-count', (0, HEIGHT - 30))
    screen.draw.text(str(eieren_verzameld),
                     fontsize=40,
                     pos=(30, HEIGHT - 30),
                     color=FONT_KLEUR)
    screen.blit('life-count', (60, HEIGHT - 30))
    screen.draw.text(str(levens),
                     fontsize=40,
                     pos=(90, HEIGHT - 30),
                     color=FONT_KLEUR)
    screen.draw.text(str(levens),
                     fontsize=40,
                     pos=(90, HEIGHT - 30),
                     color=FONT_KLEUR)


def update():
    if keyboard.right:
        held.x += BEWEEG_AFSTAND
        if held.x > WIDTH:
            held.x = WIDTH
    elif keyboard.left:
        held.x -= BEWEEG_AFSTAND
        if held.x < 0:
            held.x = 0
    elif keyboard.down:
        held.y += BEWEEG_AFSTAND
        if held.y > HEIGHT:
            held.y = HEIGHT
    elif keyboard.up:
        held.y -= BEWEEG_AFSTAND
        if held.y < 0:
            held.y = 0
    check_voor_raken()


def update_grotten():
    global grotten, held, levens
    for grot in grotten:
        if grot['draak'].image == 'dragon-asleep':
            update_slapende_draak(grot)
        elif grot['draak'].image == 'dragon-awake':
            update_wakkere_draak(grot)
        update_ei(grot)


clock.schedule_interval(update_grotten, 1)


def update_slapende_draak(grot):
    if grot['slaap_teller'] >= grot['slaap_lengte']:
        grot['draak'].image = 'dragon-awake'
        grot['slaap_teller'] = 0
    else:
        grot['slaap_teller'] += 1


def update_wakkere_draak(grot):
    if grot['wakker_teller'] >= DRAAK_WEKTIJD:
        grot['draak'].image = 'dragon-asleep'
        grot['wakker_teller'] = 0
    else:
        grot['wakker_teller'] += 1


def update_ei(grot):
    if grot['ei_verborgen'] is True:
        if grot['ei_verberg_teller'] >= EIEREN_VERBERGTIJD:
            grot['ei_verborgen'] = False
            grot['ei_verberg_teller'] = 0
        else:
            grot['ei_verberg_teller'] += 1


def check_voor_raken():
    global grotten, eieren_verzameld, levens, reset_vereist, game_voltooid
    for grot in grotten:
        if grot['ei_verborgen'] is False:
            check_voor_ei_raken(grot)
        if grot['draak'].image == 'dragon-awake' and reset_vereist is False:
            check_voor_draak_raken(grot)


def check_voor_draak_raken(grot):
    x_afstand = held.x - grot['draak'].x
    y_afstand = held.y - grot['draak'].y
    afstand = math.hypot(x_afstand, y_afstand)
    if afstand < AANVAL_AFSTAND:
        handle_draak_raken()


def handle_draak_raken():
    global reset_vereist
    reset_vereist = True
    animate(held, pos=HELD_START, on_finished=verminder_leven)


def check_voor_ei_raken(grot):
    global eieren_verzameld, game_voltooid
    if held.colliderect(grot['eieren']):
        grot['ei_verborgen'] = True
        eieren_verzameld += grot['ei_tellen']
        if eieren_verzameld >= EIEREN_DOEL:
            game_voltooid = True


def verminder_leven():
    global levens, reset_vereist, game_over
    levens -= 1
    if levens == 0:
        game_over = True
    reset_vereist = False
