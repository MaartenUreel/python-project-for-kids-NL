from random import randint

WIDTH = 800
HEIGHT = 600

ballon = Actor('balloon')
ballon.pos = 400, 300

vogel = Actor('bird-up')
vogel.pos = randint(800, 1600), randint(10, 200)

huis = Actor('house')
huis.pos = randint(800, 1600), 460

boom = Actor('tree')
boom.pos = randint(800, 1600), 450

vogel_omhoog = True
omhoog = False
game_over = False
score = 0
aantal_updates = 0

scores = []


def update_hoogste_scores():
    global score, scores
    bestandsnaam = r'C:\Projects\highscores.txt'
    scores = []
    with open(bestandsnaam, 'r') as bestand:
        lijn = bestand.readline()
        hoogste_scores = lijn.split()
        for hoogste_scores in hoogste_scores:
            if(score > int(hoogste_scores)):
                scores.append(str(score) + ' ')
                score = int(hoogste_scores)
            else:
                scores.append(str(hoogste_scores) + ' ')
    with open(bestandsnaam, 'w') as bestand:
        for hoogste_scores in scores:
            bestand.write(hoogste_scores)


def toon_hoogste_scores():
    screen.draw.text('HOOGSTE SCORES', (350, 150), color='black')
    y = 175
    positie = 1
    for hoogste_score in scores:
        screen.draw.text(str(positie) + '. ' + hoogste_score, (350, y), color='black')
        y += 25
        positie += 1


def draw():
    screen.blit('background', (0, 0))
    if not game_over:
        ballon.draw()
        vogel.draw()
        huis.draw()
        boom.draw()
        screen.draw.text('Score: ' + str(score), (700, 5), color='black')
    else:
        toon_hoogste_scores()


def on_mouse_down():
    global omhoog
    omhoog = True
    ballon.y -= 50


def on_mouse_up():
    global omhoog
    omhoog = False


def fladder():
    global vogel_omhoog
    if vogel_omhoog:
        vogel.image = 'bird-down'
        vogel_omhoog = False
    else:
        vogel.image = 'bird-up'
        vogel_omhoog = True


def update():
    global game_over, score, aantal_updates
    if not game_over:
        if not omhoog:
            ballon.y += 1

        if vogel.x > 0:
            vogel.x -= 4
            if aantal_updates == 9:
                fladder()
                aantal_updates = 0
            else:
                aantal_updates += 1
        else:
            vogel.x = randint(800, 1600)
            vogel.y = randint(10, 200)
            score += 1
            aantal_updates = 0

        if huis.right > 0:
            huis.x -= 2
        else:
            huis.x = randint(800, 1600)
            score += 1

        if boom.right > 0:
            boom.x -= 2
        else:
            boom.x = randint(800, 1600)
            score += 1

        if ballon.top < 0 or ballon.bottom > 560:
            game_over = True
            update_hoogste_scores()

        if ballon.collidepoint(vogel.x, vogel.y) or \
           ballon.collidepoint(huis.x, huis.y) or \
           ballon.collidepoint(boom.x, boom.y):
            game_over = True
            update_hoogste_scores()
