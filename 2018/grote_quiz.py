WIDTH = 1280
HEIGHT = 720

hoofd_vak = Rect(0, 0, 820, 240)
timer_vak = Rect(0, 0, 240, 240)
antwoord_vak1 = Rect(0, 0, 495, 165)
antwoord_vak2 = Rect(0, 0, 495, 165)
antwoord_vak3 = Rect(0, 0, 495, 165)
antwoord_vak4 = Rect(0, 0, 495, 165)

hoofd_vak.move_ip(50, 40)
timer_vak.move_ip(990, 40)
antwoord_vak1.move_ip(50, 358)
antwoord_vak2.move_ip(735, 358)
antwoord_vak3.move_ip(50, 538)
antwoord_vak4.move_ip(735, 538)
antwoord_vakken = [antwoord_vak1, antwoord_vak2, antwoord_vak3, antwoord_vak4]

score = 0
tijd_over = 10

v1 = ['Wat is de hoofdstad van Frankrijk?',
      'Londen', 'Parijs', 'Berlijn', 'Tokyo', 2]

v2 = ['Wat is 5+7?',
      '12', '10', '14', '8', 1]

v3 = ['Wat is de zevende maand van het jaar?',
      'April', 'Mei', 'Juni', 'Juli', 4]


v4 = ['Welke planeet staat het dichtst bij de zon?',
      'Saturnus', 'Neptunus', 'Mercurius', 'Venus', 3]


v5 = ['Waar liggen de piramiden?',
      'India', 'Egypte', 'Marokko', 'Canada', 2]

vragen = [v1, v2, v3, v4, v5]
vraag = vragen.pop(0)


def draw():
    screen.fill('dim grey')
    screen.draw.filled_rect(hoofd_vak, 'sky blue')
    screen.draw.filled_rect(timer_vak, 'sky blue')

    for vak in antwoord_vakken:
        screen.draw.filled_rect(vak, 'orange')
    screen.draw.textbox(str(tijd_over), timer_vak, color=('black'))
    screen.draw.textbox(vraag[0], hoofd_vak, color=('black'))

    index = 1
    for vak in antwoord_vakken:
        screen.draw.textbox(vraag[index], vak, color=('black'))
        index = index + 1


def game_over():
    global vraag, tijd_over
    boodschap = 'Game over. Je hebt %s vragen juist' % str(score)
    vraag = [boodschap, '-', '-', '-', '-', 5]
    tijd_over = 0


def juist_antwoord():
    global vraag, score, tijd_over

    score = score + 1
    if vragen:
        vraag = vragen.pop(0)
        tijd_over = 10
    else:
        print('Einde van de vragen')
        game_over()


def on_mouse_down(pos):
    index = 1
    for vak in antwoord_vakken:
        if vak.collidepoint(pos):
            print('Geklikt op antwoord' + str(index))
            if index == vraag[5]:
                print('Je hebt het goed!')
                juist_antwoord()
            else:
                game_over()
        index = index + 1


def update_tijd_over():
    global tijd_over

    if tijd_over:
        tijd_over = tijd_over - 1
    else:
        game_over()


clock.schedule_interval(update_tijd_over, 1.0)
