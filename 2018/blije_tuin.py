from random import randint
import time

WIDTH = 800
HEIGHT = 600
MIDDEN_X = WIDTH / 2
MIDDEN_Y = HEIGHT / 2

game_over = False
beeindigd = False
tuin_blij = True
vangbloem_raken = False

tijd_verstreken = 0
start_tijd = time.time()

koe = Actor('cow')
koe.pos = 100, 500

bloem_lijst = []
verwelkt_lijst = []
vangbloem_lijst = []
vangbloem_vy_lijst = []
vangbloem_vx_lijst = []


def draw():
    global game_over, tijd_verstreken, beeindigd
    if not game_over:
        screen.clear()
        screen.blit('garden', (0, 0))
        koe.draw()
        for bloem in bloem_lijst:
            bloem.draw()
        for vangbloem in vangbloem_lijst:
            vangbloem.draw()
        tijd_verstreken = int(time.time() - start_tijd)
        screen.draw.text(
            'Tuin blij gedurende: ' +
            str(tijd_verstreken) + ' seconden',
            topleft=(10, 10), color='black'
        )
    else:
        if not beeindigd:
            koe.draw()
            screen.draw.text(
                'Tuin blij gedurende: ' +
                str(tijd_verstreken) + ' seconden',
                topleft=(10, 10), color='black'
            )
            if (not tuin_blij):
                screen.draw.text(
                    'TUIN NIET BLIJ - GAME OVER!', color='black',
                    topleft=(10, 50)
                )
                beeindigd = True
            else:
                screen.draw.text(
                    'VANGBLOEM AANVAL - GAME OVER!', color='black',
                    topleft=(10, 50)
                )
                beeindigd = True
    return


def nieuwe_bloem():
    global bloem_lijst, verwelkt_lijst
    bloem_nieuw = Actor('flower')
    bloem_nieuw.pos = randint(50, WIDTH - 50), randint(150, HEIGHT - 100)
    bloem_lijst.append(bloem_nieuw)
    verwelkt_lijst.append('blij')
    return


def toevoegen_bloemen():
    global game_over
    if not game_over:
        nieuwe_bloem()
        clock.schedule(toevoegen_bloemen, 4)
    return


def check_verwelk_tijden():
    global verwelkt_lijst, game_over, tuin_blij
    if verwelkt_lijst:
        for verwelkt_sinds in verwelkt_lijst:
            if not verwelkt_sinds == 'blij':
                tijd_verwelkt = int(time.time() - verwelkt_sinds)
                if (tijd_verwelkt) > 10.0:
                    tuin_blij = False
                    game_over = True
                    break
    return


def verwelk_bloem():
    global bloem_lijst, verwelkt_lijst, game_over
    if not game_over:
        if bloem_lijst:
            rand_bloem = randint(0, len(bloem_lijst) - 1)
            if (bloem_lijst[rand_bloem].image == 'flower'):
                bloem_lijst[rand_bloem].image = 'flower-wilt'
                verwelkt_lijst[rand_bloem] = time.time()
        clock.schedule(verwelk_bloem, 3)
    return


def check_bloem_raken():
    global koe, bloem_lijst, verwelkt_lijst
    index = 0
    for bloem in bloem_lijst:
        if (bloem.colliderect(koe) and
                bloem.image == 'flower-wilt'):
            bloem.image = 'flower'
            verwelkt_lijst[index] = 'blij'
            break
        index = index + 1
    return


def check_vangbloem_raken():
    global koe, vangbloem_lijst, vangbloem_raken
    global game_over
    for vangbloem in vangbloem_lijst:
        if vangbloem.colliderect(koe):
            koe.image = 'zap'
            game_over = True
            break
    return


def snelheid():
    random_richting = randint(0, 1)
    random_snelheid = randint(2, 3)
    if random_richting == 0:
        return -random_snelheid
    else:
        return random_snelheid


def muteer():
    global bloem_lijst, vangbloem_lijst, vangbloem_vy_lijst
    global vangbloem_vx_lijst, game_over
    if not game_over and bloem_lijst:
        rand_bloem = randint(0, len(bloem_lijst) - 1)
        vangbloem_pos_x = bloem_lijst[rand_bloem].x
        vangbloem_pos_y = bloem_lijst[rand_bloem].y
        del bloem_lijst[rand_bloem]
        vangbloem = Actor('fangflower')
        vangbloem.pos = vangbloem_pos_x, vangbloem_pos_y
        vangbloem_vx = snelheid()
        vangbloem_vy = snelheid()
        vangbloem = vangbloem_lijst.append(vangbloem)
        vangbloem_vx_lijst.append(vangbloem_vx)
        vangbloem_vy_lijst.append(vangbloem_vy)
        clock.schedule(muteer, 20)
    return


def update_vangbloemen():
    global vangbloem_lijst, game_over
    if not game_over:
        index = 0
        for vangbloem in vangbloem_lijst:
            vangbloem_vx = vangbloem_vx_lijst[index]
            vangbloem_vy = vangbloem_vy_lijst[index]
            vangbloem.x = vangbloem.x + vangbloem_vx
            vangbloem.y = vangbloem.y + vangbloem_vy
            if vangbloem.left < 0:
                vangbloem_vx_lijst[index] = -vangbloem_vx
            if vangbloem.right > WIDTH:
                vangbloem_vx_lijst[index] = -vangbloem_vx
            if vangbloem.top < 150:
                vangbloem_vy_lijst[index] = -vangbloem_vy
            if vangbloem.bottom > HEIGHT:
                vangbloem_vy_lijst[index] = -vangbloem_vy
            index = index + 1
    return


def reset_koe():
    global game_over
    if not game_over:
        koe.image = 'cow'
    return


toevoegen_bloemen()
verwelk_bloem()


def update():
    global score, game_over, vangbloem_raken
    global bloem_lijst, vangbloem_lijst, tijd_verstreken
    vangbloem_raken = check_vangbloem_raken()
    check_verwelk_tijden()
    if not game_over:
        if keyboard.space:
            koe.image = 'cow-water'
            clock.schedule(reset_koe, 0.5)
            check_bloem_raken()
        if keyboard.left and koe.x > 0:
            koe.x -= 5
        elif keyboard.right and koe.x < WIDTH:
            koe.x += 5
        elif keyboard.up and koe.y > 150:
            koe.y -= 5
        elif keyboard.down and koe.y < HEIGHT:
            koe.y += 5
        if tijd_verstreken > 15 and not vangbloem_lijst:
            muteer()
        update_vangbloemen()
