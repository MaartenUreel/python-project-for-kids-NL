import random
import time
from tkinter import Tk, Canvas, HIDDEN, NORMAL

def volgende_vorm():
    global vorm
    global vorige_kleur
    global huidige_kleur
    
    vorige_kleur = huidige_kleur
    
    c.delete(vorm)
    if len(vormen) > 0:
        vorm = vormen.pop()
        c.itemconfigure(vorm, state=NORMAL)
        huidige_kleur = c.itemcget(vorm, 'fill')
        root.after(1000, volgende_vorm)
    else:
        c.unbind('q')
        c.unbind('p')
        if speler1_score > speler2_score:
            c.create_text(200, 200, text='Winnaar: Speler 1')
        elif speler2_score > speler1_score:
            c.create_text(200, 200, text='Winnaar: Speler 2')
        else:
            c.create_text(200, 200, text='Gelijkspel')
        c.pack()

def snap(event):
    global vorm
    global speler1_score
    global speler2_score
    geldig = False

    c.delete(vorm)
    if vorige_kleur == huidige_kleur:
        geldig = True

    if geldig:
        if event.char == 'q':
            speler1_score = speler1_score + 1
        else:
            speler2_score = speler2_score + 1
        vorm = c.create_text(200, 200, text='SNAP! Je hebt 1 punt gescoord!')
    else:
        if event.char == 'q':
            speler1_score = speler1_score - 1
        else:
            speler2_score = speler2_score - 1
        vorm = c.create_text(200, 200, text='FOUT! Je hebt 1 punt verloren!')
    c.pack()
    root.update_idletasks()
    time.sleep(1)

root = Tk()
root.title('Snap')
c = Canvas(root, width=400, height=400)

vormen = []

cirkel = c.create_oval(35, 20, 365, 350, outline='black', fill='black', state=HIDDEN)
vormen.append(cirkel)
cirkel = c.create_oval(35, 20, 365, 350, outline='red', fill='red', state=HIDDEN)
vormen.append(cirkel)
cirkel = c.create_oval(35, 20, 365, 350, outline='green', fill='green', state=HIDDEN)
vormen.append(cirkel)
cirkel = c.create_oval(35, 20, 365, 350, outline='blue', fill='blue', state=HIDDEN)
vormen.append(cirkel)

rechthoek = c.create_rectangle(35, 100, 365, 270, outline='black', fill='black', state=HIDDEN)
vormen.append(rechthoek)
rechthoek = c.create_rectangle(35, 100, 365, 270, outline='red', fill='red', state=HIDDEN)
vormen.append(rechthoek)
rechthoek = c.create_rectangle(35, 100, 365, 270, outline='green', fill='green', state=HIDDEN)
vormen.append(rechthoek)
rechthoek = c.create_rectangle(35, 100, 365, 270, outline='blue', fill='blue', state=HIDDEN)
vormen.append(rechthoek)

vierkant = c.create_rectangle(35, 20, 365, 350, outline='black', fill='black', state=HIDDEN)
vormen.append(vierkant)
vierkant = c.create_rectangle(35, 20, 365, 350, outline='red', fill='red', state=HIDDEN)
vormen.append(vierkant)
vierkant = c.create_rectangle(35, 20, 365, 350, outline='green', fill='green', state=HIDDEN)
vormen.append(vierkant)
vierkant = c.create_rectangle(35, 20, 365, 350, outline='blue', fill='blue', state=HIDDEN)
vormen.append(vierkant)
c.pack()

random.shuffle(vormen)

vorm = None
vorige_kleur = ''
huidige_kleur = ''
speler1_score = 0
speler2_score = 0

root.after(3000, volgende_vorm)
c.bind('q', snap)
c.bind('p', snap)
c.focus_set()

root.mainloop()