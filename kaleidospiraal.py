import turtle
from itertools import cycle

kleuren = cycle(['red', 'orange', 'yellow', \
                 'green', 'blue', 'purple'])

def teken_cirkel(grootte, hoek, draaiing):
    turtle.pencolor(next(kleuren))
    turtle.circle(grootte)
    turtle.right(hoek)
    turtle.forward(draaiing)
    teken_cirkel(grootte + 5, hoek + 1, draaiing + 1)

turtle.bgcolor('black')
turtle.speed('fast')
turtle.pensize(4)
teken_cirkel(30, 0, 1)