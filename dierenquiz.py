def check_raad(raad, antwoord):
    global score
    nog_radend = True
    poging = 0
    while nog_radend and poging < 3:
        if raad.lower() == antwoord.lower():
            print('Correct antwoord')
            score = score + 1
            nog_radend = False
        else:
            if poging < 2:
                raad = input('Sorry, verkeerd antwoord. Probeer opnieuw ')
            poging = poging + 1

    if poging == 3:
        print('Het correcte antwoord is ' + antwoord)

score = 0
print('Raad het dier')
raad1 = input('Welke beer leeft op de Noordpool? ')
check_raad(raad1, 'ijsbeer')
raad2 = input('Wat is het snelste landdier? ')
check_raad(raad2, 'luipaard')
raad3 = input('Welk dier is het grootst? ')
check_raad(raad3, 'blauwe vinvis')

print('Jouw score is ' + str(score))